# French translation of PSmisc messages.
# Copyright (C) 2014 Free Software Foundation, Inc.
# This file is distributed under the same license as the psmisc package.
#
# Marc Léger <sorata@users.sourceforge.net>, 2001.
# Benno Schulenberg <benno@vertaalt.nl>, 2007.
# Frédéric Marchal <fmarchal@perso.be>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: psmisc-22.21-pre2\n"
"Report-Msgid-Bugs-To: csmall@enc.com.au\n"
"POT-Creation-Date: 2015-06-30 23:01+1000\n"
"PO-Revision-Date: 2014-02-02 13:40+0100\n"
"Last-Translator: Frédéric Marchal <fmarchal@perso.be>\n"
"Language-Team: French <traduc@traduc.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms:  nplurals=2; plural=(n > 1);\n"

#: src/fuser.c:123
#, fuzzy, c-format
msgid ""
"Usage: fuser [-fMuvw] [-a|-s] [-4|-6] [-c|-m|-n SPACE] [-k [-i] [-SIGNAL]] "
"NAME...\n"
"       fuser -l\n"
"       fuser -V\n"
"Show which processes use the named files, sockets, or filesystems.\n"
"\n"
"  -a,--all              display unused files too\n"
"  -i,--interactive      ask before killing (ignored without -k)\n"
"  -I,--inode            use always inodes to compare files\n"
"  -k,--kill             kill processes accessing the named file\n"
"  -l,--list-signals     list available signal names\n"
"  -m,--mount            show all processes using the named filesystems or "
"block device\n"
"  -M,--ismountpoint     fulfill request only if NAME is a mount point\n"
"  -n,--namespace SPACE  search in this name space (file, udp, or tcp)\n"
"  -s,--silent           silent operation\n"
"  -SIGNAL               send this signal instead of SIGKILL\n"
"  -u,--user             display user IDs\n"
"  -v,--verbose          verbose output\n"
"  -w,--writeonly        kill only processes with write access\n"
"  -V,--version          display version information\n"
msgstr ""
"Utilisation : fuser [-fMuvw] [-a|-s] [-4|-6] [-c|-m|-n ESPACE] [-k [-i] [-"
"SIGNAL]] NOM...\n"
"              fuser -l\n"
"              fuser -V\n"
"Affiche les processus utilisant les fichiers, sockets ou systèmes de "
"fichiers nommés.\n"
"\n"
"  -a,--all              montre les fichiers inutilisés\n"
"  -i,--interactive      demande avant de fermer un processus (ignoré sans -"
"k)\n"
"  -k,--kill             ferme les processus accédant au fichier spécifié\n"
"  -l,--list-signals     liste les noms des signaux\n"
"  -m,--mount            affiche les processus utilisant les systèmes de "
"fichiers\n"
"                        ou les périphériques blocs spécifiés\n"
"  -M,--ismountpoint     exécute la requête seulement si NOM est un point de "
"montage\n"
"  -n,--namespace ESPACE cherche dans l'espace de noms spécifiés (file, udp "
"ou tcp)\n"
"  -s,--silent           mode silencieux\n"
"  -SIGNAL               envoie ce signal au lieu de SIGKILL\n"
"  -u,--user             affiche la liste des utilisateurs\n"
"  -v,--verbose          mode bavard\n"
"  -w,--writeonly        ferme uniquement les processus avec la permission en "
"écriture\n"
"  -V,--version          affiche des informations sur la version\n"

#: src/fuser.c:141
#, c-format
msgid ""
"  -4,--ipv4             search IPv4 sockets only\n"
"  -6,--ipv6             search IPv6 sockets only\n"
msgstr ""
"  -4,--ipv4             cherche des sockets IPv4 seulement\n"
"  -6,--ipv6             cherche des sockets IPv6 seulement\n"

#: src/fuser.c:144
#, c-format
msgid ""
"  -                     reset options\n"
"\n"
"  udp/tcp names: [local_port][,[rmt_host][,[rmt_port]]]\n"
"\n"
msgstr ""
"  -                     réinitialise les options\n"
"\n"
"  noms udp/tcp: [port_local][,[hôte_distant][,[port_distant]]]\n"
"\n"

#: src/fuser.c:151
#, c-format
msgid "fuser (PSmisc) %s\n"
msgstr "fuser (PSmisc) %s\n"

#: src/fuser.c:154
#, c-format
msgid ""
"Copyright (C) 1993-2010 Werner Almesberger and Craig Small\n"
"\n"
msgstr ""
"Copyright (C) 1993-2010 Werner Almesberger et Craig Small\n"
"\n"

#: src/fuser.c:156 src/killall.c:702 src/peekfd.c:114 src/prtstat.c:68
#: src/pstree.c:1054
#, c-format
msgid ""
"PSmisc comes with ABSOLUTELY NO WARRANTY.\n"
"This is free software, and you are welcome to redistribute it under\n"
"the terms of the GNU General Public License.\n"
"For more information about these matters, see the files named COPYING.\n"
msgstr ""
"PSmisc ne fait l'objet d'AUCUNE GARANTIE.\n"
"C'est un logiciel libre, et vous pouvez le redistribuer\n"
"en respectant les termes de la licence GNU General Public License.\n"
"Pour plus de précisions à ce sujet, voir les fichiers nommés COPYING.\n"

#: src/fuser.c:175
#, c-format
msgid "Cannot open /proc directory: %s\n"
msgstr "Impossible d'ouvrir le répertoire /proc: %s\n"

#: src/fuser.c:366 src/fuser.c:419 src/fuser.c:1874
#, c-format
msgid "Cannot allocate memory for matched proc: %s\n"
msgstr "Impossible d'allouer de la mémoire au processus correspondant: %s\n"

#: src/fuser.c:446
#, c-format
msgid "Specified filename %s does not exist.\n"
msgstr "Le nom de fichier %s n'existe pas.\n"

#: src/fuser.c:449
#, c-format
msgid "Cannot stat %s: %s\n"
msgstr "Impossible d'obtenir les stat de %s: %s\n"

#: src/fuser.c:586
#, c-format
msgid "Cannot resolve local port %s: %s\n"
msgstr "Ne peut résoudre le port local %s: %s\n"

#: src/fuser.c:604
#, c-format
msgid "Unknown local port AF %d\n"
msgstr "Port local AF inconnu %d\n"

#: src/fuser.c:692
#, c-format
msgid "Cannot open protocol file \"%s\": %s\n"
msgstr "Impossible d'ouvrir le fichier protocole « %s »: %s\n"

#: src/fuser.c:878
#, c-format
msgid "Specified filename %s is not a mountpoint.\n"
msgstr "Le nom de fichier %s n'est pas un point de montage.\n"

#: src/fuser.c:978
#, c-format
msgid "%s: Invalid option %s\n"
msgstr "%s: Option %s incorrecte\n"

#: src/fuser.c:1035
msgid "Namespace option requires an argument."
msgstr "L'option « --namespace » requiert un paramètre."

#: src/fuser.c:1053
msgid "Invalid namespace name"
msgstr "Paramètre incorrect pour « --namespace »"

#: src/fuser.c:1118
msgid "You can only use files with mountpoint options"
msgstr ""
"Vous ne pouvez utiliser que des fichiers avec les options de point de montage"

#: src/fuser.c:1167
msgid "No process specification given"
msgstr "Aucune spécification de processus donnée"

#: src/fuser.c:1179
msgid "all option cannot be used with silent option."
msgstr "l'option « --all » ne peut être utilisée avec l'option « --silent »"

#: src/fuser.c:1184
msgid "You cannot search for only IPv4 and only IPv6 sockets at the same time"
msgstr ""
"Vous ne pouvez pas rechercher simultanément et exclusivement les sockets "
"IPv4 et IPv6"

#: src/fuser.c:1263
#, c-format
msgid "%*s USER        PID ACCESS COMMAND\n"
msgstr "%*s UTIL.       PID ACCÈS  COMMANDE\n"

#: src/fuser.c:1296 src/fuser.c:1353
msgid "(unknown)"
msgstr "(inconnu)"

#: src/fuser.c:1432 src/fuser.c:1471
#, c-format
msgid "Cannot stat file %s: %s\n"
msgstr "Impossible d'obtenir les stat du fichier %s: %s\n"

#: src/fuser.c:1557
#, c-format
msgid "Cannot open /proc/net/unix: %s\n"
msgstr "Impossible d'ouvrir /proc/net/unix : %s\n"

#: src/fuser.c:1633
#, c-format
msgid "Kill process %d ? (y/N) "
msgstr "Tuer le processus %d ? (y/N) "

#: src/fuser.c:1669
#, c-format
msgid "Could not kill process %d: %s\n"
msgstr "Impossible de tuer le processus %d : %s\n"

#: src/fuser.c:1684
#, c-format
msgid "Cannot open a network socket.\n"
msgstr "Impossible d'ouvrir un socket réseau.\n"

#: src/fuser.c:1688
#, c-format
msgid "Cannot find socket's device number.\n"
msgstr "Impossible de trouver le numéro du périphérique des sockets\n"

#: src/killall.c:106
#, c-format
msgid "Kill %s(%s%d) ? (y/N) "
msgstr "Tuer %s(%s%d) ? (y/N) "

#: src/killall.c:109
#, c-format
msgid "Signal %s(%s%d) ? (y/N) "
msgstr "Envoyer signal %s(%s%d) ? (y/N) "

#: src/killall.c:216
#, c-format
msgid "killall: Cannot get UID from process status\n"
msgstr "killall: Impossible d'obtenir l'UID à partir de l'état du processus\n"

#: src/killall.c:242
#, c-format
msgid "killall: Bad regular expression: %s\n"
msgstr "killall: Mauvaise expression régulière : %s\n"

#: src/killall.c:376
#, c-format
msgid "killall: skipping partial match %s(%d)\n"
msgstr "killall: Ignore la correspondance partielle %s(%d)\n"

#: src/killall.c:603
#, c-format
msgid "Killed %s(%s%d) with signal %d\n"
msgstr "Tué %s(%s%d) avec le signal %d\n"

#: src/killall.c:623
#, c-format
msgid "%s: no process found\n"
msgstr "%s: aucun processus trouvé\n"

#: src/killall.c:664
#, c-format
msgid ""
"Usage: killall [-Z CONTEXT] [-u USER] [ -eIgiqrvw ] [ -SIGNAL ] NAME...\n"
msgstr ""
"Usage : killall [-Z CONTEXTE] [-u UTILISATEUR] [ -eIgiqrvw ] [ -SIGNAL ] "
"NOM...\n"

#: src/killall.c:667
#, c-format
msgid "Usage: killall [OPTION]... [--] NAME...\n"
msgstr "Usage : killall [OPTION]... [--] NOM...\n"

#: src/killall.c:670
#, c-format
msgid ""
"       killall -l, --list\n"
"       killall -V, --version\n"
"\n"
"  -e,--exact          require exact match for very long names\n"
"  -I,--ignore-case    case insensitive process name match\n"
"  -g,--process-group  kill process group instead of process\n"
"  -y,--younger-than   kill processes younger than TIME\n"
"  -o,--older-than     kill processes older than TIME\n"
"  -i,--interactive    ask for confirmation before killing\n"
"  -l,--list           list all known signal names\n"
"  -q,--quiet          don't print complaints\n"
"  -r,--regexp         interpret NAME as an extended regular expression\n"
"  -s,--signal SIGNAL  send this signal instead of SIGTERM\n"
"  -u,--user USER      kill only process(es) running as USER\n"
"  -v,--verbose        report if the signal was successfully sent\n"
"  -V,--version        display version information\n"
"  -w,--wait           wait for processes to die\n"
msgstr ""
"       killall -l, --list\n"
"       killall -V, --version\n"
"\n"
"  -e,--exact            requiert une concordance parfaite des noms très "
"longs\n"
"  -I,--ignore-case      recherche en ignorant la casse\n"
"  -g,--process-group    tue le groupe associé au programme au lieu de celui-"
"ci\n"
"  -y,--younger-than     tue les programmes créés avant HEURE\n"
"  -o,--older-than       tue les programmes créés après HEURE\n"
"  -i,--interactive      demande une confirmation avant de tuer\n"
"  -l,--list             affiche tous les noms de signaux connus\n"
"  -q,--quiet            n'affiche pas les remarques\n"
"  -r,--regexp           interprète NOM comme une expression régulière "
"étendue\n"
"  -s,--signal SIGNAL    envoie ce signal au lieu de SIGTERM\n"
"  -u,--user UTILISATEUR ne tue que le(s) programme(s) utilisé(s) par "
"UTILISATEUR\n"
"  -v,--verbose          informe si le signal a été correctement envoyé\n"
"  -V,--version          affiche les informations sur la version\n"
"  -w,--wait             attend que les programmes s'arrêtent\n"

#: src/killall.c:688
#, c-format
msgid ""
"  -Z,--context REGEXP kill only process(es) having context\n"
"                      (must precede other arguments)\n"
msgstr ""
"  -Z,--context REGEXP ferme seulement le(s) processus ayant l'argument "
"context\n"
"                      (doit précéder les autres arguments)\n"

#: src/killall.c:700
#, fuzzy, c-format
msgid ""
"Copyright (C) 1993-2014 Werner Almesberger and Craig Small\n"
"\n"
msgstr ""
"Copyright (C) 1993-2010 Werner Almesberger et Craig Small\n"
"\n"

#: src/killall.c:790 src/killall.c:796
msgid "Invalid time format"
msgstr "Format de temps incorrect"

#: src/killall.c:816
#, c-format
msgid "Cannot find user %s\n"
msgstr "Utilisateur %s introuvable\n"

#: src/killall.c:847
#, c-format
msgid "Bad regular expression: %s\n"
msgstr "Mauvaise expression régulière : %s\n"

#: src/killall.c:879
#, c-format
msgid "killall: Maximum number of names is %d\n"
msgstr "killall: Le nombre maximum de noms est %d\n"

#: src/killall.c:884
#, c-format
msgid "killall: %s lacks process entries (not mounted ?)\n"
msgstr "killall: %s n'a aucune entrée de processus (pas monté ?)\n"

#: src/peekfd.c:102
#, c-format
msgid "Error attaching to pid %i\n"
msgstr "Erreur lors de l'attachement au pid %i\n"

#: src/peekfd.c:110
#, c-format
msgid "peekfd (PSmisc) %s\n"
msgstr "peekfd (PSmisc) %s\n"

#: src/peekfd.c:112
#, c-format
msgid ""
"Copyright (C) 2007 Trent Waddington\n"
"\n"
msgstr ""
"Copyright (C) 2007 Trent Waddington\n"
"\n"

#: src/peekfd.c:122
#, c-format
msgid ""
"Usage: peekfd [-8] [-n] [-c] [-d] [-V] [-h] <pid> [<fd> ..]\n"
"    -8 output 8 bit clean streams.\n"
"    -n don't display read/write from fd headers.\n"
"    -c peek at any new child processes too.\n"
"    -d remove duplicate read/writes from the output.\n"
"    -V prints version info.\n"
"    -h prints this help.\n"
"\n"
"  Press CTRL-C to end output.\n"
msgstr ""
"Usage: peekfd [-8] [-n] [-c] [-d] [-V] [-h] <pid> [<fd> ..]\n"
"    -8 écrit des caractères sur 8 bits dans la sortie.\n"
"    -n n'affiche pas les lectures/écritures dans les en-têtes fd.\n"
"    -c examine également chaque nouveau processus fils.\n"
"    -d ignore les lectures/écritures dupliquées.\n"
"    -V affiche la version.\n"
"    -h affiche cet aide-mémoire.\n"
"\n"
"  Appuyez sur CTRL-C pour interrompre.\n"

#: src/prtstat.c:54
#, c-format
msgid ""
"Usage: prtstat [options] PID ...\n"
"       prtstat -V\n"
"Print information about a process\n"
"    -r,--raw       Raw display of information\n"
"    -V,--version   Display version information and exit\n"
msgstr ""
"Usage: prtstat [options] PID ...\n"
"       prtstat -V\n"
"Affiche les informations au sujet d'un processus\n"
"    -r,--raw       Affiche les informations brutes\n"
"    -V,--version   Affiche la version et ne fait rien d'autre\n"

#: src/prtstat.c:65
#, c-format
msgid "prtstat (PSmisc) %s\n"
msgstr "prtstat (PSmisc) %s\n"

#: src/prtstat.c:66
#, c-format
msgid ""
"Copyright (C) 2009 Craig Small\n"
"\n"
msgstr ""
"Copyright (C) 2009 Craig Small\n"
"\n"

#: src/prtstat.c:78
msgid "running"
msgstr "en cours"

#: src/prtstat.c:80
msgid "sleeping"
msgstr "endormi"

#: src/prtstat.c:82
msgid "disk sleep"
msgstr "attente disque"

#: src/prtstat.c:84
msgid "zombie"
msgstr "zombie"

#: src/prtstat.c:86
msgid "traced"
msgstr "tracé"

#: src/prtstat.c:88
msgid "paging"
msgstr "demande de page"

#: src/prtstat.c:90
msgid "unknown"
msgstr "inconnu"

#: src/prtstat.c:164
#, c-format
msgid ""
"Process: %-14s\t\tState: %c (%s)\n"
"  CPU#:  %-3d\t\tTTY: %s\tThreads: %ld\n"
msgstr ""
"Processus: %-14s\t\tÉtat: %c (%s)\n"
"  CPU#:  %-3d\t\tTTY: %s\tThreads: %ld\n"

#: src/prtstat.c:169
#, c-format
msgid ""
"Process, Group and Session IDs\n"
"  Process ID: %d\t\t  Parent ID: %d\n"
"    Group ID: %d\t\t Session ID: %d\n"
"  T Group ID: %d\n"
"\n"
msgstr ""
"Processus, Groupe et ID de session\n"
"  ID processus: %d\t\t  ID parent: %d\n"
"     ID groupe: %d\t\t ID session: %d\n"
"   ID groupe T: %d\n"
"\n"

#: src/prtstat.c:175
#, c-format
msgid ""
"Page Faults\n"
"  This Process    (minor major): %8lu  %8lu\n"
"  Child Processes (minor major): %8lu  %8lu\n"
msgstr ""
"Erreurs de page\n"
"  Ce processus   (mineur majeur): %8lu  %8lu\n"
"  Processus fils (mineur majeur): %8lu  %8lu\n"

#: src/prtstat.c:180
#, c-format
msgid ""
"CPU Times\n"
"  This Process    (user system guest blkio): %6.2f %6.2f %6.2f %6.2f\n"
"  Child processes (user system guest):       %6.2f %6.2f %6.2f\n"
msgstr ""
"Temps CPU\n"
"  Ce processus   (utilisateur système invité blkio): %6.2f %6.2f %6.2f "
"%6.2f\n"
"  Processus fils (utilisateur système invitéà:       %6.2f %6.2f %6.2f\n"

#: src/prtstat.c:189
#, c-format
msgid ""
"Memory\n"
"  Vsize:       %-10s\n"
"  RSS:         %-10s \t\t RSS Limit: %s\n"
"  Code Start:  %#-10lx\t\t Code Stop:  %#-10lx\n"
"  Stack Start: %#-10lx\n"
"  Stack Pointer (ESP): %#10lx\t Inst Pointer (EIP): %#10lx\n"
msgstr ""
"Mémoire\n"
"  Vsize:      %-10s\n"
"  RSS:        %-10s \t\t Limite RSS: %s\n"
"  Début code: %#-10lx\t\t Fin code:  %#-10lx\n"
"  Début pile: %#-10lx\n"
"  Pointeur pile (ESP): %#10lx\t Pointeur inst. (EIP): %#10lx\n"

#: src/prtstat.c:199
#, c-format
msgid ""
"Scheduling\n"
"  Policy: %s\n"
"  Nice:   %ld \t\t RT Priority: %ld %s\n"
msgstr ""
"Ordonancement\n"
"  Politique:  %s\n"
"  Courtoisie: %ld \t\t Priorité TR: %ld %s\n"

#: src/prtstat.c:220
msgid "asprintf in print_stat failed.\n"
msgstr "échec de asprintf dans print_stat.\n"

#: src/prtstat.c:225
#, c-format
msgid "Process with pid %d does not exist.\n"
msgstr "Le processus avec le pid %d n'existe pas.\n"

#: src/prtstat.c:227
#, c-format
msgid "Unable to open stat file for pid %d (%s)\n"
msgstr "Impossible d'ouvrir le fichier stat du pid %d (%s)\n"

#: src/prtstat.c:311
msgid "Invalid option"
msgstr "Option incorrecte"

#: src/prtstat.c:316
msgid "You must provide at least one PID."
msgstr "Vous devez spécifier au moins un PID."

#: src/prtstat.c:320
#, c-format
msgid "/proc is not mounted, cannot stat /proc/self/stat.\n"
msgstr ""
"/proc n'est pas monté, impossible d'obtenir les stat de /proc/self/stat.\n"

#: src/pstree.c:984
#, c-format
msgid "%s is empty (not mounted ?)\n"
msgstr "%s est vide (pas monté ?)\n"

#: src/pstree.c:1016
#, fuzzy, c-format
msgid ""
"Usage: pstree [ -a ] [ -c ] [ -h | -H PID ] [ -l ] [ -n ] [ -p ] [ -g ] [ -"
"u ]\n"
"              [ -A | -G | -U ] [ PID | USER ]\n"
"       pstree -V\n"
"Display a tree of processes.\n"
"\n"
"  -a, --arguments     show command line arguments\n"
"  -A, --ascii         use ASCII line drawing characters\n"
"  -c, --compact       don't compact identical subtrees\n"
"  -h, --highlight-all highlight current process and its ancestors\n"
"  -H PID,\n"
"  --highlight-pid=PID highlight this process and its ancestors\n"
"  -g, --show-pgids    show process group ids; implies -c\n"
"  -G, --vt100         use VT100 line drawing characters\n"
"  -l, --long          don't truncate long lines\n"
"  -n, --numeric-sort  sort output by PID\n"
"  -N type,\n"
"  --ns-sort=type      sort by namespace type (ipc, mnt, net, pid, user, "
"uts)\n"
"  -p, --show-pids     show PIDs; implies -c\n"
"  -s, --show-parents  show parents of the selected process\n"
"  -S, --ns-changes    show namespace transitions\n"
"  -t, --thread-names  show full thread names\n"
"  -u, --uid-changes   show uid transitions\n"
"  -U, --unicode       use UTF-8 (Unicode) line drawing characters\n"
"  -V, --version       display version information\n"
msgstr ""
"Usage: pstree [ -a ] [ -c ] [ -h | -H PID ] [ -l ] [ -n ] [ -p ] [ -g ] [ -"
"u ]\n"
"              [ -A | -G | -U ] [ PID | UTILISATEUR ]\n"
"       pstree -V\n"
"Affiche l'arborescence des processus.\n"
"\n"
"  -a, --arguments     afficher les paramètres de la ligne de commande\n"
"  -A, --ascii         utiliser les caractères de tracé ASCII\n"
"  -c, --compact       ne pas grouper des branches identiques\n"
"  -h, --highlight-all surligner le processus courant et ses parents\n"
"  -H PID,\n"
"  --highlight-pid=PID surligner le processus spécifié et ses parents\n"
"  -g, --show-pgids    afficher les ID des groupes du processus (implique -"
"c)\n"
"  -G, --vt100         utiliser les caractères de tracé VT100\n"
"  -l, --long          ne pas tronquer les longues lignes\n"
"  -n, --numeric-sort  trier le résultat par PID\n"
"  -N type,\n"
"  --ns-sort=type      trier par type d'espace de nom (ipc, mnt, net, pid, "
"user, uts)\n"
"  -p, --show-pids     afficher les PIDs (implique -c)\n"
"  -s, --show-parents  afficher les parents du processus sélectionné\n"
"  -S, --ns-changes    afficher les transitions d'espaces de noms\n"
"  -u, --uid-changes   montrer les transitions de uid\n"
"  -U, --unicode       utiliser les caractères de tracé UTF-8 (Unicode)\n"
"  -V, --version       afficher les informations sur la version\n"

#: src/pstree.c:1040
#, c-format
msgid "  -Z     show         SELinux security contexts\n"
msgstr "  -Z     show         montrer les contextes de sécurité SELinux\n"

#: src/pstree.c:1042
#, c-format
msgid ""
"  PID    start at this PID; default is 1 (init)\n"
"  USER   show only trees rooted at processes of this user\n"
"\n"
msgstr ""
"  PID    commence à ce PID; le défaut est 1 (init)\n"
"  USER   montre seulement les arbres nichés aux processus de cet "
"utilisateur\n"
"\n"

#: src/pstree.c:1049
#, c-format
msgid "pstree (PSmisc) %s\n"
msgstr "pstree (PSmisc) %s\n"

#: src/pstree.c:1052
#, c-format
msgid ""
"Copyright (C) 1993-2009 Werner Almesberger and Craig Small\n"
"\n"
msgstr ""
"Copyright (C) 1993-2009 Werner Almesberger et Craig Small\n"
"\n"

#: src/pstree.c:1167
#, c-format
msgid "TERM is not set\n"
msgstr "TERM n'est pas défini\n"

#: src/pstree.c:1171
#, c-format
msgid "Can't get terminal capabilities\n"
msgstr "Impossible d'obtenir les spécifications du terminal\n"

#: src/pstree.c:1189
#, c-format
msgid "procfs file for %s namespace not available\n"
msgstr "fichier procfs indisponible pour l'espace de nom %s\n"

#: src/pstree.c:1239
#, c-format
msgid "No such user name: %s\n"
msgstr "Aucun utilisateur portant ce nom: %s\n"

#: src/pstree.c:1265
#, c-format
msgid "No processes found.\n"
msgstr "Aucun processus trouvé.\n"

#: src/pstree.c:1271
#, c-format
msgid "Press return to close\n"
msgstr "Appuyez sur la touche Entrée pour fermer\n"

#: src/signals.c:84
#, c-format
msgid "%s: unknown signal; %s -l lists signals.\n"
msgstr "%s: signal inconnu; %s -l liste les signaux.\n"

#~ msgid ""
#~ "Copyright (C) 1993-2012 Werner Almesberger and Craig Small\n"
#~ "\n"
#~ msgstr ""
#~ "Copyright (C) 1993-2012 Werner Almesberger et Craig Small\n"
#~ "\n"

#~ msgid ""
#~ "Usage: pidof [ -eg ] NAME...\n"
#~ "       pidof -V\n"
#~ "\n"
#~ "    -e      require exact match for very long names;\n"
#~ "            skip if the command line is unavailable\n"
#~ "    -g      show process group ID instead of process ID\n"
#~ "    -V      display version information\n"
#~ "\n"
#~ msgstr ""
#~ "Usage : pidof [ -eg ] NOM...\n"
#~ "        pidof -V\n"
#~ "\n"
#~ "    -e   requiert une concordance parfaite des noms très longs;\n"
#~ "         sauter si la ligne de commande est indisponible\n"
#~ "    -g   montre l'ID du groupe associé au lieu de celui du programme\n"
#~ "    -V   affiche les informations sur la version\n"
#~ "\n"

#~ msgid ""
#~ "Copyright (C) 1993-2005 Werner Almesberger and Craig Small\n"
#~ "\n"
#~ msgstr ""
#~ "Copyright (C) 1993-2005 Werner Almesberger et Craig Small\n"
#~ "\n"
